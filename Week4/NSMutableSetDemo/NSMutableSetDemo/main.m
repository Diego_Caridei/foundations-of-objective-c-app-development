//
//  main.m
//  NSMutableSetDemo
//
//  Created by Guybrush_Treepwood on 27/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {

    NSSet *kidsEatDesserts =[NSSet setWithObjects:@"cookies",@"apple pie",@"banana cake",
                             @"cookies",@"pixie sticks",nil];
    
    NSSet *grandmaMakesTheseDesserts =[NSSet setWithObjects:@"apple pie",@"banana cake",
                                       @"jello",nil];
    NSSet *dadMakeTheseDesserts=[NSSet setWithObjects:@"jello",@"toast", nil];
    
    NSMutableSet *possibleDesserts =[NSMutableSet setWithCapacity:1];
    [possibleDesserts setSet:grandmaMakesTheseDesserts];
    [possibleDesserts unionSet:dadMakeTheseDesserts];
    NSLog(@"\n Here's what can be made for dessert: %@",possibleDesserts);
    
    [possibleDesserts intersectSet:kidsEatDesserts];
    NSLog(@"\n all the possible desserts this is what will kid eat %@",possibleDesserts);
    
    [possibleDesserts setSet:grandmaMakesTheseDesserts];
    [possibleDesserts unionSet:dadMakeTheseDesserts];
    [possibleDesserts minusSet:kidsEatDesserts];
    NSLog(@"\n the kids won't wat these %@",possibleDesserts);
    return 0;
}
