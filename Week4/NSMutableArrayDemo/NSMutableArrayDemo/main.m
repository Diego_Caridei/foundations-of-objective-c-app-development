//
//  main.m
//  NSMutableArrayDemo
//
//  Created by Guybrush_Treepwood on 27/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSMutableArray *kidsAteTHeseDesserts =[NSMutableArray arrayWithObjects:@"cookies",@"apple pie",@"banana cake", @"cookies",@"pixie sticks",nil];
    NSMutableArray *dadMadeTheseDesserts =[NSMutableArray arrayWithObjects:@"jello",@"toast",nil];
    [kidsAteTHeseDesserts addObject:@"toast"];
    NSLog(@"\nHere's what the kids ate: %@",kidsAteTHeseDesserts);
    [kidsAteTHeseDesserts removeAllObjects];
    NSLog(@"\nHere's what the kids ate: %@",kidsAteTHeseDesserts);
    [dadMadeTheseDesserts insertObject:@"pixie stick" atIndex:1];
    NSLog(@"\nHere's what the dad  made: %@",dadMadeTheseDesserts);
    [dadMadeTheseDesserts removeObjectAtIndex:0];
    NSLog(@"\nHere's what the dad  made: %@",dadMadeTheseDesserts);
    [dadMadeTheseDesserts replaceObjectAtIndex:1 withObject:@"toast flavored"];
    NSLog(@"\nHere's what the dad  made: %@",dadMadeTheseDesserts);
    [dadMadeTheseDesserts removeObject:@"pixie stick"];
    NSLog(@"\nHere's what the dad  made: %@",dadMadeTheseDesserts);

    return 0;
}
