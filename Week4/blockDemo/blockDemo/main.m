//
//  main.m
//  blockDemo
//
//  Created by Guybrush_Treepwood on 29/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    //example 1
    NSLog(@"\nExample 1\n");
    NSMutableDictionary *dinnerRequest=[NSMutableDictionary dictionaryWithDictionary:@{@"Don":@"tofurkey",@"Sandy":@"burrito",@"Julius":@"chicken",@"Theo":@"hamburger"}];
    [dinnerRequest enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
        NSLog(@"Key: %@, Value: %@\n",key,value);
    }];
    
    //example 2
    NSLog(@"\nExample 2\n");

    NSArray *family = @[@"Don",@"Sandy",@"Kyle",@"Nico",@"Mike",@"Jack"];
    [family enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isEqualToString:@"Nico"]) {
            *stop=true;
        }else{
            NSLog(@"%@\n",obj);
        }
        
    }];
    
    //example 3
    NSLog(@"\nExample 3\n");
    //return blockname type
    void (^blockName)(int,int)=^(int x,int y){
        NSLog(@"%d",x+y);
    };
    blockName(2,3);
    
    //example 4
    NSLog(@"\nExample 4\n");
    void (^myBlock)(id,NSUInteger,BOOL *);
    myBlock = ^(id obj,NSUInteger index,BOOL *stop){
        if ([obj isEqualToString:@"Nico"]) {
            *stop=YES;
        }else{
            NSLog(@"%@\n",obj);
        }
    };
    [family enumerateObjectsUsingBlock:myBlock];
    
    
    //example 5
    NSLog(@"\nExample 5\n");
    NSMutableString *(^myBlock2)(NSInteger,NSString*);
    myBlock2 = ^(NSInteger count,NSString *repeatMe){
        NSMutableString *local =[NSMutableString stringWithString:@"\n"];
        for (int i=0; i<count; i++) {
            [local appendString:repeatMe];
        }
        return local;
    };
    
    NSMutableString *answer = myBlock2(3,@" There is a magic number");
    NSLog(@"%@",answer);
    
    
    //Example 6 using an external variable in a block
    NSLog(@"\nExample 6\n");

    NSMutableArray *collector = [NSMutableArray arrayWithCapacity:1];
    void(^myBlock3)(id,NSUInteger,BOOL*);
    myBlock3=^(id obj, NSUInteger Index, BOOL *stop){
        if ([[obj description]containsString:@"D"]) {
            [collector addObject:obj];
        }
    };
    [family enumerateObjectsUsingBlock:myBlock3];
    NSLog(@"%@",collector);
  
    


    return 0;
}
