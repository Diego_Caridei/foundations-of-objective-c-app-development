//
//  CarPart.h
//  InheritanceDemo
//
//  Created by Guybrush_Treepwood on 29/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import "Product.h"

@interface CarPart : Product
@property(nonatomic)NSString *carModel;
@property(nonatomic)NSString *oem;

@end
