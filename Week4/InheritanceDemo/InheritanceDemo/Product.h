//
//  Product.h
//  InheritanceDemo
//
//  Created by Guybrush_Treepwood on 29/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject
@property(nonatomic)NSString *sku;
@property(nonatomic )NSString *supplier;
@end
