//
//  main.m
//  NSArrayDemo
//
//  Created by Guybrush_Treepwood on 26/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
  
    NSArray *dinners =@[@"spaghetti",@"tacos",@"tofurkey",
                        @"hamburgers",@"chili",@"fish"];
    NSArray *desserts =[NSArray arrayWithObjects:@"apple pie",
                        @"banana cake",@"cookies",@"pixie sticks", nil];
    
    NSLog(@"First dinner %@\n",dinners[0]);
    NSLog(@"First dessert %@",[desserts objectAtIndex:0]);
    
    for(NSString *i in dinners){
        NSLog(@"%@\n",i);
    }
    for (int i=0; i<[desserts count]; i++) {
        NSLog(@"%d: %@",i,desserts[i]);
    }
    
    
    if ([dinners isEqualToArray:desserts]) {
        NSLog(@"dinners and desserts are equal\n");
    }else{
        NSLog(@"dinners and desserts are  not equal\n");

    }
    
    NSUInteger index =[dinners indexOfObject:@"tofurkey"];
    if (index==NSNotFound) {
        NSLog(@"Tofurkey isn't in our list of dinners");
    }else{
        NSLog(@"Tofurkey is in on the dinner menu at index %ld",index);

    }
    
    
    NSArray *sortedDinners =[dinners sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSArray *sortedDesserts = [desserts sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    for (NSString *x in sortedDinners) {
        NSLog(@"%@\n",x);
    }
    for (NSString *x in sortedDesserts) {
        NSLog(@"%@\n",x);
    }
    
    NSArray *allFood = [dinners arrayByAddingObjectsFromArray:desserts];
    NSArray * sortedAllFood =[allFood sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    for (NSString *x in sortedAllFood) {
        NSLog(@"%@\n",x);
    }
    
    return 0;
}
