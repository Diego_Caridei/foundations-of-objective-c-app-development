//
//  main.m
//  NSDictionaryDemo
//
//  Created by Guybrush_Treepwood on 27/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {

    //First object second key
    NSDictionary *dinnerRequest =@{
                                   @"Don":@"torfurkey",
                                   @"Sandy":@"burrito",
                                   @"Julius":@"chicken",
                                   @"Theo":@"hamburger",
                                   @"Joy":@"pixie sticks",
                                   @"Coconut":@"burrito",
                                   };
    NSLog(@"%@\n",dinnerRequest);
    NSLog(@"Theo wants %@ for dinner\n",dinnerRequest[@"Theo"]);
    NSLog(@"Joy wants %@ for dinner\n",dinnerRequest[@"Joy"]);
    NSArray *burritosWanter=[dinnerRequest allKeysForObject:@"burrito"];
    for (id x in burritosWanter){
        NSLog(@"%@\n",x);
    }
    
    //Print all
    NSLog(@"Key and value\n");
    for (id i in dinnerRequest){
        //i = key dinnerRequest[i]= value
        NSLog(@"%@ %@",i,dinnerRequest[i]);
    }
    
    //Count how mucj request
    for(id v in [dinnerRequest allValues]){
        NSLog(@"%@ request by %lu\n",v,[[dinnerRequest allKeysForObject:v]count]);
    }
    
    
    
    NSArray *people =@[@"Don",@"Sandy",@"Julius",@"Theo",@"Joy",@"Martha",@"Coconut"];
    NSArray *request =@[@"tofurkey",@"burrito",@"chicken",@"hamburger",@"burrito",@"pixie sticks",@"kibble"];
    NSDictionary *dinnerRequest2=[NSDictionary dictionaryWithObject:request forKey:people];
    NSLog(@"%@",dinnerRequest2);
    
    
    return 0;
}
