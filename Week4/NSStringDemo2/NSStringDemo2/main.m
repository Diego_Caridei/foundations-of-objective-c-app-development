//
//  main.m
//  NSStringDemo2
//
//  Created by Guybrush_Treepwood on 26/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {

    NSString *timeOfDay=@"morning";
    NSNumber *a = @1321414121;
    NSString *format =@"Good %@, you have %@ credits left";
    NSString *output=[NSString stringWithFormat:format,timeOfDay,a];
    
    printf("%s\n",[output UTF8String]);
    printf("Lenght is %lu\n",[output length]);
    printf("First character is %c\n",[output characterAtIndex:0]);
    printf("Six character is %c\n",[output characterAtIndex:5]);
    printf("0 - 6 is %s\n",[[output substringWithRange:NSMakeRange(0, 6)]UTF8String]);
    
    if ([output hasSuffix:@"left"]) {
        printf("It does end with \"left\"\n");
    }else{
        printf("It doesn't end with \"left\"\n");

    }
    
    return 0;
}
