//
//  main.m
//  NSStringDemo
//
//  Created by Guybrush_Treepwood on 26/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    char *timeOfDay="morning";
    long a = 124412131;
    NSString *format = [NSString stringWithFormat:@"Good %s, you have %ld credits left\n",timeOfDay,a];
    NSString *third = [NSString stringWithFormat:format,timeOfDay,a];
    NSLog(@"%@",third);
    
    NSNumber *num =@2;
    NSString *numNSString = [num description];
    char *numString =[numNSString UTF8String];
    printf("This is the result %s\n",numString);
    
    NSNumber *num2 =@2;
    NSString *numNSString2 = [NSString stringWithFormat:@"This is the result %@\n",num2];
    char *numString2 =[numNSString2 UTF8String];
    printf("This is the result %s\n",numString2);
    
    
    return 0;
}
