//
//  main.m
//  NSSetDemo
//
//  Created by Guybrush_Treepwood on 26/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    //Example 1
   /*
    NSSet *dinners =[NSSet setWithObjects:@"fish",@"spaghetti",@"tacos",
                     @"tofurkey",@"hamburgers",@"chili",@"fish", nil];
    NSSet *desserts =[NSSet setWithObjects:@"cookies",@"apple pie",
                      @"banana cake",@"cookies",@"pixie sticks", nil];
    
    NSSet *allFood = [NSSet setWithObjects:dinners,desserts,@"toast", nil];
    NSLog(@"dinners\n");
    for(NSString *x in dinners){
        NSLog(@"%@\n",x);
    }
    NSLog(@"desserts\n");
    for(NSString *x in desserts){
        NSLog(@"%@\n",x);
    }
    */
    
   //Example 2
    /*
    NSArray *dinners =@[@"fish",@"spaghetti",@"tacos", @"tofurkey",
                        @"hamburgers",@"chili",@"fish"];
    NSSet *dinnersSet =[NSSet setWithArray:dinners];
    
    NSLog(@"dinners\n");
    for (NSString *x in dinners){
        NSLog(@"%@\n",x);
    }
    NSLog(@"dinnersSet\n");
    for (NSString *x in dinnersSet){
        NSLog(@"%@\n",x);
    }
    */
    
    //Example 3
    /*
    NSSet *dinners =[NSSet setWithObjects:@"fish",@"spaghetti",@"tacos",
                     @"tofurkey",@"hamburgers",@"chili",@"fish", nil];
    NSSet *desserts =[NSSet setWithObjects:@"cookies",@"apple pie",
                      @"banana cake",@"cookies",@"pixie sticks", nil];
    NSNumber *example =@3.14;
    NSSet *allFood =[NSSet setWithObjects:dinners,desserts,@"toast",example, nil];
    NSLog(@"AllFood\n");
    for(id x in allFood){
        NSLog(@"%@\n",x);

    }
    */
     //Example 4
    NSSet *kidsEatDesserts =[NSSet setWithObjects:@"cookies",@"apple pie",@"banana cake"
                             ,@"cookies",@"pixie sticks",nil];
    
    NSSet *grandmaMakesTheseDesserts =[NSSet setWithObjects:@"apple pie",@"banana cake",
                                       @"jello",nil];
    NSSet *dadMakesTHeDesserts = [NSSet setWithObjects:@"jello", nil];
    
    if ([dadMakesTHeDesserts isEqual:grandmaMakesTheseDesserts]) {
        NSLog(@"Dad and Grandma make the exactly the same desserts\n");
    }else{
        NSLog(@"Dad and Grandma don't make the exactly the same desserts\n");

    }
    
    if ([kidsEatDesserts intersectsSet:dadMakesTHeDesserts]) {
        NSLog(@"The kids eat a dessert that dad makes\n");
    }else{
        NSLog(@"The kids don't eat a dessert that dad makes\n");

    }
    
    if ([dadMakesTHeDesserts isSubsetOfSet:grandmaMakesTheseDesserts]) {
        NSLog(@"Grandma makes all the desserts that dad makes");
    }else{
        NSLog(@"Grandma doesn't make all the desserts that dad makes");

    }
    
    return 0;
}
