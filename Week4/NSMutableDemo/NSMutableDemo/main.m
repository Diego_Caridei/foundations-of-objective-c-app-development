//
//  main.m
//  NSMutableDemo
//
//  Created by Guybrush_Treepwood on 27/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    //NsmutableString example 1
    NSString *a=@"First example";
    NSString *b=@"Second example";
    NSString *c=@"Third example";
    
    NSString *immutable;
    immutable=a;
    immutable=b;
    immutable=c;
    NSLog(@"%@",immutable);//Third example

    NSMutableString *mutable =[[NSMutableString alloc]init];
    [mutable setString:a];
    [mutable setString:b];
    [mutable setString:c];
    NSLog(@"%@",immutable);
    
    //NsmutableString example 2
    NSMutableString *company =[NSMutableString stringWithCapacity:20];
    NSString *product =@"Macbook Pro";
    [company setString:@"Apple"];
    [company appendString:@" "];
    [company appendString:product];
    NSLog(@"%@\n",company);
    
    [company setString:@"Apple"];
    [company appendFormat:@" %@ inch %@",@17,product];
    NSLog(@"%@\n",company);
    
    [company setString:@"Apple Macbook Pro"];
    [company insertString:@"14 inch" atIndex:6];
    NSLog(@"%@\n",company);

    //NsmutableString example 3
    NSMutableString *products =[NSMutableString stringWithCapacity:20];
    [products setString:@"Apple MacBook Pro"];
    NSLog(@"%@\n",products);
    [products replaceCharactersInRange:NSMakeRange(6, 7) withString:@"iPad"];
    NSLog(@"%@\n",products);
    [products deleteCharactersInRange:NSMakeRange(10, 4)];
    NSLog(@"%@\n",products);


    

    

    return 0;
}
