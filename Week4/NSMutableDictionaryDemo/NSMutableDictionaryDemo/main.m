//
//  main.m
//  NSMutableDictionaryDemo
//
//  Created by Guybrush_Treepwood on 27/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {

    /*
     Example 1
    NSMutableDictionary *dinnerRequest =[NSMutableDictionary dictionaryWithDictionary:@{@"Don":@"rofurkey",@"Sandy":@"burrito",@"Julius":@"chicken",@"Theo":@"hamburcer",@"Coconut":@"kibble",}];
    
    [dinnerRequest setObject:@"pixie sticks" forKey:@"Martha"];
    [dinnerRequest removeObjectForKey:@"Coconut"];
    dinnerRequest[@"Joy"]=@"burrito";
    NSLog(@"\n%@",dinnerRequest);
    
     */
    
    NSMutableDictionary *dinnerRequests =[NSMutableDictionary dictionaryWithDictionary:@{@"Don":@"rofurkey",@"Sandy":@"burrito",@"Julius":@"chicken",@"Theo":@"hamburcer",@"Coconut":@"kibble",}];
    NSMutableDictionary *moreRequests=[NSMutableDictionary dictionaryWithDictionary:@{@"Joy":@"burrito",@"Dusty":@"millet"}];
    [dinnerRequests addEntriesFromDictionary:moreRequests];
    NSLog(@"\n%@",dinnerRequests);
  
    
    return 0;
}
