//
//  main.m
//  Structs
//
//  Created by Guybrush_Treepwood on 21/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef struct{
    int moth;
    int day;
    int year ;
    int hour;
    int minutes ;
    int seconds ;
}AnotherTime;

struct time{
    int moth;
    int day;
    int year ;
    int hour;
    int minutes ;
    int seconds ;
};

struct biography{
    struct time birth;
    struct time graduation;
    struct time marriage;
    struct time death;
};
struct Anotherbiography{
    AnotherTime birth;
    AnotherTime gruation;
    AnotherTime marriage;
    AnotherTime death;
};


void timeGreeting(int moth,int day,int year,int hour,int minutes,int seconds){
    printf("The day is %d/%d/%d, the time is %d:%d:%d\n",moth,day,year,hour,minutes,seconds);
}

void timeGreetingStruct(struct time myTime){
    printf("The day is %d/%d/%d, the time is %d:%d:%d\n",myTime.moth,myTime.day,myTime.year,myTime.hour,myTime.minutes,myTime.seconds);
}
int main(int argc, const char * argv[]) {
    /*
    int moth =6;
    int day=29;
    int year =2007;
    int hour=16;
    int minutes =30;
    int seconds = 00;
    timeGreeting(moth, day, year, hour, minutes, seconds);
    */
    struct time myTime;
    struct biography myBiography;
    struct Anotherbiography anotherBio;
    myTime.moth =6;
    myTime.year = 2007;
    myTime.hour=16;
    myTime.minutes =30;
    myTime.seconds = 00;
    timeGreetingStruct(myTime);
    
    myBiography.birth.year = 1973;
    anotherBio.birth.year=1980;
    return 0;
}
