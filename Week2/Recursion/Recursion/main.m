//
//  main.m
//  Recursion
//
//  Created by Guybrush_Treepwood on 21/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

int timesTwoQuickly(int number){
    return 2*number;
}
int timesTwoWithRecrsuion(int number){
    if (number==0) {
        return 0;
    }else{
        return 2+timesTwoWithRecrsuion(number-1);
    }
}


int main(int argc, const char * argv[]) {

    int A = timesTwoQuickly(3);
    printf("timesTwoQuickly %d\n",A);
    int B = timesTwoWithRecrsuion(3);
    printf("timesTwoWithRecrsuion %d\n",B);
    
    return 0;
}
