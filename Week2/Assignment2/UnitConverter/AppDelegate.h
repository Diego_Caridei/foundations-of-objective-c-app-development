//
//  AppDelegate.h
//  UnitConverter
//
//  Created by Guybrush_Treepwood on 22/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

