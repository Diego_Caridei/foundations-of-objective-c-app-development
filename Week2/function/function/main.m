//
//  main.m
//  function
//
//  Created by Guybrush_Treepwood on 20/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>


void goodDay(char *timeOfDay){
    printf("Good %s\n",timeOfDay);
}

void allDay(){
    goodDay("morning");
    goodDay("afternoon");
    goodDay("evening");
}

void dayGreeting(int loops){
    for (int i=0; i<loops; i++) {
        allDay();
    }
}



int main(int argc, const char * argv[]) {
  

    dayGreeting(5);
    return 0;
}
