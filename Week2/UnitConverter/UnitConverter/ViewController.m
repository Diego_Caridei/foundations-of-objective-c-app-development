//
//  ViewController.m
//  UnitConverter
//
//  Created by Guybrush_Treepwood on 22/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import "ViewController.h"



/*
double convertUnitOneToUnitTwo (double unirOneValue){
    double unitTwo;
    unitTwo =10 *unirOneValue+2;
    return unitTwo;
}
 */
double inchesToFeet(double inches){
    return inches/12;
}
double inchesToMeters(double inches){
    return inches/39.370;
}
double inchesToMiles (double inches){
    return inchesToMeters(inches)/1609.344;
}
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *inputField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentController;
@property (weak, nonatomic) IBOutlet UILabel *outputLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)updateButton:(id)sender {
    
    double usetInput = [self.inputField.text doubleValue];
    
    NSMutableString *buf = [NSMutableString new];
    if (self.segmentController.selectedSegmentIndex==0) {
        double unitThoeValue = inchesToFeet (usetInput);
        [buf appendString:[@(unitThoeValue) stringValue]];
         
    }
     else if (self.segmentController.selectedSegmentIndex==1) {
         double unitThoeValue = inchesToMeters (usetInput);
         [buf appendString:[@(unitThoeValue) stringValue]];
     }
     else  {
         double unitThoeValue = inchesToMiles (usetInput);
         [buf appendString:[@(unitThoeValue) stringValue]];
     }
    
    self.outputLabel.text =buf;
}



@end
