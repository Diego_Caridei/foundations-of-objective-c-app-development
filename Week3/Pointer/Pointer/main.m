//
//  main.m
//  Pointer
//
//  Created by Guybrush_Treepwood on 24/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>
void addFive(int *x){
    *x=*x+5;
}


int main(int argc, const char * argv[]) {
    int x =2;
    printf("at first x is %d\n",x);
    addFive(&x);
    printf("at least x is %d\n",x);

    return 0;
}
