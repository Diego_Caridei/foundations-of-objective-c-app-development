//
//  main.m
//  Property
//
//  Created by Guybrush_Treepwood on 24/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyTime.h"
int main(int argc, const char * argv[]) {
   
    MyTime *time =[[MyTime alloc]init];
    //Use automatic setter method
    [time setHour:10];
    [time setMinutes:20];
    
    int newHour,newMinutes;
    //Use the getter method
    newHour = [time hour];
    newMinutes = [time minutes];
    return 0;
}
