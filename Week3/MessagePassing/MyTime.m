//
//  MyTime.m
//  MessagePassing
//
//  Created by Guybrush_Treepwood on 24/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import "MyTime.h"

@implementation MyTime

-(long)approxSecondsSincheWichYead:(long)wichYear{
    long allsec=0;
    allsec=self.seconds;
    allsec=allsec+self.minutes*60;
    allsec=allsec+self.hour*60*60;
    allsec=allsec+self.day*24*60*60;
    allsec=allsec+self.month*30*24*60*60;
    allsec=allsec+(self.year-wichYear)*365*30*24*60*60;
    return allsec;
}
-(long)approxSecondsSinche1970{
    return [self approxSecondsSincheWichYead:1970];
}

-(long)secondsSincheMidnight{
    long x;
    x=self.seconds;
    x=x+self.minutes*60;
    x=x+self.hour*60*60;
    return x;
}

-(int)addSomeParameters:(int)a secondParameter:(int)b thirdParameter:(int)c{
    int answer;
    answer=self.hour;
    answer=answer+a+b+c;
    return answer;
}

@end
