//
//  MyTime.h
//  MessagePassing
//
//  Created by Guybrush_Treepwood on 24/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyTime : NSObject
@property (nonatomic)int year;
@property (nonatomic)int month;
@property (nonatomic)int day;
@property (nonatomic)int hour;
@property (nonatomic)int minutes;
@property (nonatomic)int seconds;

-(long)approxSecondsSincheWichYead:(long)wichYear;
-(long)approxSecondsSinche1970;
-(long)secondsSincheMidnight;
-(int)addSomeParameters:(int)a secondParameter: (int)b thirdParameter:(int)c;
@end
