//
//  main.m
//  MessagePassing
//
//  Created by Guybrush_Treepwood on 24/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyTime.h"

int main(int argc, const char * argv[]) {
   
    MyTime *time= [[MyTime alloc]init];
    time.hour=10;
    int answer;
    answer=[time addSomeParameters:1 secondParameter:2 thirdParameter:3];
    printf("The answer is: %d\n",answer);
    
    //long x = [a approxSecondsSinche1970];
    
    return 0;
}
