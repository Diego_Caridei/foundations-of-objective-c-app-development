//
//  ViewController.m
//  Assignment3
//
//  Created by Guybrush_Treepwood on 24/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import "ViewController.h"
#import "CurrencyRequest/CRCurrencyRequest.h"
#import "CurrencyRequest/CRCurrencyResults.h"

@interface ViewController ()<CRCurrencyRequestDelegate>
@property(nonatomic)CRCurrencyRequest *req;
@property (weak, nonatomic) IBOutlet UILabel *currencyB;
@property (weak, nonatomic) IBOutlet UILabel *currencyC;
@property (weak, nonatomic) IBOutlet UILabel *currencyA;
@property (weak, nonatomic) IBOutlet UIButton *convertButton;
@property (weak, nonatomic) IBOutlet UITextField *inputField;
@end

@implementation ViewController

- (IBAction)buttonTapped:(id)sender {
    self.convertButton.enabled =NO;
    self.req = [[CRCurrencyRequest alloc]init];
    self.req.delegate=self;
    [self.req start];
    
}

-(void)currencyRequest:(CRCurrencyRequest *)req retrievedCurrencies:(CRCurrencyResults *)currencies{
    
    self.convertButton.enabled =YES;
    double inputValue = [self.inputField.text floatValue];
    double euroValue = inputValue * currencies.EUR;
    double jpnValue = inputValue * currencies.JPY;
    double gbpValue = inputValue *currencies.GBP;
    self.currencyA.text = [NSString stringWithFormat:@"%.2f",euroValue];
    self.currencyB.text = [NSString stringWithFormat:@"%.2f",jpnValue];
    self.currencyC.text = [NSString stringWithFormat:@"%.2f",gbpValue];

}

@end
