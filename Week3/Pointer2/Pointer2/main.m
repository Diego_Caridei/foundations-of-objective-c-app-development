//
//  main.m
//  Pointer2
//
//  Created by Guybrush_Treepwood on 24/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <Foundation/Foundation.h>
int *a = NULL;
void swapItOut(int c){
    printf("swapItOut: a is now %p and points to %d\n",a,(*a));
    a=(&c);
    printf("swapItOut: a is now %p and points to %d\n",a,(*a));
}

void dontSwapItOut(int c){
    printf("dontSwapItOut: c is n %d\n",c);
}

int main(int argc, const char * argv[]) {
    int b=5;
    a=(&b);
    printf("main: a is now %p and points to %d\n",a,(*a));
    swapItOut(10);
    printf("main: a is now %p and points to %d\n",a,(*a));
    dontSwapItOut(20);
    printf("main: a is now %p and points to %d\n",a,(*a));
    int *c=NULL;
    do{
        c=malloc(sizeof(int));
        free(c);
    }while (c!=NULL);
    return 0;
}
