# Foundations of Objective-C App Development#

![ejpt_certificate_sm.png](https://lh6.googleusercontent.com/-uESEAsyyBIQ/AAAAAAAAAAI/AAAAAAAAAAA/gF-pknP_1r0/s0-c-k-no-ns/photo.jpg)

#Link
https://www.coursera.org/learn/objective-c

#Module 1
Welcome! 

Course Syllabus

Pre-Course Survey

Tools 

#Module 2
Your First App 

Xcode Tour 

What programming does

Peer Review Assignment

Peer Review 

Peer Review Assignment 

Assignment: Assignment 

#Module 3

Variables and Types 

Conditionals 

Command-line project 

Loops 

Quiz: Week 1 Quiz10 questions

#Module 4
Functions 1 

Functions 2 

Scope 

Frames and Recursion 

#Module 5
Video Lecture for Module 5

Structs 

Peer Review Assignment

Peer Review 02 

Peer Review Assignment # 2 – Unit Conversion App

Assignment: Peer Review Assignment #2 - Unit Conversion App

#Module 6
Video Lectures for Module 6

Objects 

Pointers 

Heap Memory 

#Module 7

Video Lectures for Module 7

Message Passing 

Properties 

Resume

Peer Review Assignment

Peer Review 03 

Peer Review Assignment # 3 – Currency Conversion App

Assignment: Peer Review Assignment #3 - Currency Conversion App

#Module 8

Start Lesson

Video Lectures in Module 8

Format Strings 

NSString 

Using NSString 

#Module 9

NSArray 

NSSet 

NSDictionary 

#Module 10
Mutables 

ARC 

Callbacks and Blocks 

Inheritance 

Peer Review Assignment

Peer Review 04 (Video)

Peer Review Assignment # 4 – Distance Calculation App

Assignment: Peer Review Assignment #4 - Distance Calculation App







