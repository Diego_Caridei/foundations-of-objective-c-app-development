//
//  ViewController.h
//  Assignment1
//
//  Created by Guybrush_Treepwood on 20/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *theLabel;

- (IBAction)tapped:(id)sender;

@end

