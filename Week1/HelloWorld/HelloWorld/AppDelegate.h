//
//  AppDelegate.h
//  HelloWorld
//
//  Created by Guybrush_Treepwood on 20/09/15.
//  Copyright © 2015 Caridei Diego Solution. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

